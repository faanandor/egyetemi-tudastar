<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="hu">
    <head>
        <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> -->
        <meta charset="UTF-8">
        <title>Egyetemi Tudástár</title>
        <link rel="stylesheet" href="res/style.css">
        <link rel="stylesheet" href="res/all.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
            var rowids = [];
            var noteids = [];
            var selectedRowId = "";
            var selectedNoteId = "";
            var ratingByUser;
            
            function sendNoteIDandRatingValue() {
                var requestToSend = {"noteId": selectedNoteId, "userRating": ratingByUser};
                setTimeout(checkIfRated(), 2000);

                $.ajax({
                    url: "/EgyetemiTudastar/LoadData",
                    type: "GET",
                    data: requestToSend,
                    success: function () {
                        //alert("Siker!");
                    },
                    error: function () {
                        //alert("Hiba!");
                    }
                });
            }

            function getNotes() {
                var requestToSend = {"task": "getAllNotes"};
                var requestToSend2 = {"task": "getGlobalNetunCode"};
                var table = document.getElementById("notestable");
                var globalNepunCode;

                $.ajax({
                    url: "/EgyetemiTudastar/LoadData",
                    type: "POST",
                    data: requestToSend2,
                    success: function (response) {
                        globalNepunCode = response.neptun;
                    },
                    error: function () {
                        alert("Hiba!");
                    }
                });

                $.ajax({
                    url: "/EgyetemiTudastar/LoadData",
                    type: "POST",
                    data: requestToSend,
                    success: function (response) {
                        for (var i = 0; i < response.length; i++) {
                            var newRow = table.insertRow(i);
                            var rowId = "row" + i.toString();
                            newRow.setAttribute("id", rowId); // Mindegyik sor kap egy id-t 
                            rowids.push(rowId); // EltÃ¡roljuk a key value pÃ¡rokat egy tÃ¶mbbe
                            // TODO: Upload user rating in database
                            noteids.push(response[i].noteid);
                            console.log(response[i].noteid);
                            //var currentRow = table.rows[index];
                            //newRow.setAttribute("onclick", "alert('document.getElementById('')')");
                            var nameCol = newRow.insertCell(0);
                            var idCol = newRow.insertCell(1);
                            var dateCol = newRow.insertCell(2);
                            var fileCol = newRow.insertCell(3);
                            var neptunCol = newRow.insertCell(4);
                            var authorityCol = newRow.insertCell(5);
                            var ratingCol = newRow.insertCell(6);
                            nameCol.innerHTML = response[i].name;
                            nameCol.setAttribute("class", "idSelector");
                            newRow.setAttribute("id", rowId); // RowID az elsÅ cellÃ¡nak
                            idCol.innerHTML = response[i].subjectid;
                            dateCol.innerHTML = response[i].uploaddate;
                            fileCol.innerHTML = response[i].file;
                            neptunCol.innerHTML = response[i].userid;
                            authorityCol.innerHTML = response[i].isTeacher;
                            var numOfStars = parseInt(response[i].ratingValue); // KiszedjÃ¼k a csillagok szÃ¡mÃ¡t
                            console.log("Hello: " + numOfStars);
                            var userRating = "rating" + i.toString();
                            ratingCol.innerHTML += "<div class='rating' id='" + userRating + "'>";
                            var myDiv = document.getElementById(userRating);
                            var p = document.createElement("p");
                            var remainingStars = 5 - numOfStars;
                            if (remainingStars > 0) {
                                for (var j = 0; j < remainingStars; j++) {
                                    var myDiv = document.getElementById(userRating);
                                    var starValue = remainingStars + 1;
                                    var inpTag = document.createElement("input");
                                    var lbl = document.createElement("label");
                                    p.setAttribute("id", "starNotFilled");
                                    lbl.setAttribute("for", "starNotFilled");
                                    myDiv.appendChild(p);
                                    myDiv.appendChild(lbl);
                                }
                            }
                            if (numOfStars > 0) {
                                for (var j = 0; j < numOfStars; j++) {
                                    var myDiv = document.getElementById(userRating);
                                    var starValue = j + 1;
                                    var p = document.createElement("p");
                                    var lbl = document.createElement("label");
                                    p.setAttribute("id", "starFilled");
                                    lbl.setAttribute("for", "starFilled");
                                    myDiv.appendChild(p);
                                    myDiv.appendChild(lbl);
                                }
                            }

                            var rateCol = newRow.insertCell(7);
                            var downloadCol = newRow.insertCell(8);

                            //console.log("Neptun tablazat:" + response[i].userid + "Global: " + globalNepunCode);

                            if (response[i].userid == globalNepunCode)
                            {
                                //console.log("Neptun tablazat:" + response[i].userid + " Global: " + globalNepunCode);
                                rateCol.innerHTML += "<td><button class='deletebtn' onclick='getIDandDelete(this);'>Törlés</button></td>";
                            } else
                            {
                                rateCol.innerHTML += "<td><button class='ratingbtn' onclick='popupRatingToggle(this)'>Értékelés</button></td>";
                            }

                            var hrefForDownload = "DownloadServlet?filename=" + response[i].name + "." + response[i].file;

                            //downloadCol.innerHTML += "<td><button href='" + hrefForDownload + "' class='downloadbtn'>Letöltés</button></td>";
                            downloadCol.innerHTML += "<td><a class ='downloadbtn' href='" + hrefForDownload + "'>Letöltés</a></td>";
                        }
                    },
                    error: function () {

                    }
                });
            }
            //console.log(globalNepunCode);
            function delay(URL) {
                setTimeout(function () {
                    window.location = URL;
                }, 150);
            }
        </script>
    </head>
    <body onload="getNotes()">  
        <header> 
            <!--eleje vÃ¡ltoztatÃ¡s 1 -->
            <div class="notesh3">
                <h3>Jegyzetek</h3>
            </div>     
            <!--VÃ©ge vÃ¡ltoztatÃ¡s 1-->      
            <div class="cancel">
                <a href="fooldal.html">X</a>
            </div>
        </header>  

        <main>
            <!--eleje vÃ¡ltoztatÃ¡s 2-->
            <div class="uploaddiv">
                <button class="uploadbtn" onclick="popupToggleUpload();">Feltöltés</button>
            </div>
            <!--vÃ©ge vÃ¡ltoztatÃ¡s 2-->
            <table class="notestable">
                <thead>
                    <tr>
                        <th>Név</th>
                        <th>Kód</th>
                        <th>Dátum</th>
                        <th>Típus</th>
                        <th>Feltöltő</th>
                        <th>Tanár/Diák</th>
                        <th>Értékelés</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="notestable">
                    <!--mi ezt Ã­gy Ã­rtuk vÃ¡ltoztatÃ¡s 3 eleje-->

                    <!--
                    <tr>
                        <td>AdatbÃ¡ziSokk jegyzet</td>
                        <td>00XR45</td>
                        <td>2021.11.13</td>
                        <td>PDF</td>
                        <td>PÃ©lda Patrik</td>
                        <td>tanÃ¡r</td>
                        <td><div class="rating">
                                <p id="star1"></p>
                                <p id="star2"></p>
                                <p id="star3"></p>
                                <p id="star4"></p>
                                <p id="star5"></p>
                                <label for="star1"></label>
                                <label for="star2"></label>
                                <label for="star3"></label>
                                <label for="star4"></label>
                                <label for="star5"></label>
                            </div></td>
                        <td><button class="ratingbtn" onclick="popupRatingToggle();">ÉrtékelÃ©s</button></td>
                        <td><button class="downloadbtn">LetÃ¶ltÃ©s</button></td>
                    </tr>  
                    -->

                </tbody>
            </table>
            <!--RATING POPUP-->
            <div id="popuprating">
                <div class="content">
                    <img src="res/rating.png">
                    <h2>Értékelje a Jegyzetet</h2>
                    <p>Fontos számunkra a felhasználóktól kapott visszajelzés, ezért mondja el mennyire volt hasznos a jegyzet, segítve ezzel mások felkészülését.</p>
                    <div class="ratinginteractive">
                        <input type="radio" name="rate" id="star5">
                        <label for="star5"></label>
                        <input type="radio" name="rate" id="star4">
                        <label for="star4"></label>
                        <input type="radio" name="rate" id="star3">
                        <label for="star3"></label>
                        <input type="radio" name="rate" id="star2">
                        <label for="star2"></label>
                        <input type="radio" name="rate" id="star1">
                        <label for="star1"></label>
                    </div>
                    <div class="inputBox">
                        <input id="rateNote" type="submit" value="Értékel" class="ratingbtn" onclick="runPopupAndRating();">
                    </div>
                </div>
                <a class="close" onclick="popupRatingToggle();">
                    <img src="res/close.png">
                </a>
            </div>
            <script>

                // Lekérjük az ID-t és eltároljuk
                function getIDandToggleRating(element) {
                    popupRatingToggle(element);
                }
                function popupRatingToggle(element) {
                    const popuprating = document.getElementById('popuprating');
                    popuprating.classList.toggle('active');
                    myIndex = (element.parentNode.parentNode.rowIndex) - 1;
                    selectedRowId = "row" + myIndex.toString();
                }
                // Check if not has been rated or not
                function checkIfRated() {
                    var requestToSend = {"task": "checkIfNoteIsRated"};
                    //IDandRatingValue();
                    
                    $.ajax({
                        url: "/EgyetemiTudastar/LoadData",
                        type: "POST",
                        data: requestToSend,
                        success: function (response) {
                            if (response.canRate === "false") {
                                //console.log(response.canRate);
                                popupToggleRatingSuccess();
                                alert("Ezt a jegyzetet már értékelted!");
                            }
                        },
                        error: function () {
                            //alert("Hiba!");
                        }
                    });
                }
            </script>

            <!--RATING SUCCESS POPUP-->
            <div id="popupratingsuccess">
                <div class="content">
                    <img src="res/ok.png">
                    <h3>Sikeresen rögzítettük az értékelését!</h3>
                    <div class="inputBox">
                        <input type="submit" value="Ok" class="ratingsuccessbtn" onclick="popupToggleRatingSuccess();">
                    </div>
                </div>
            </div>
            <script>
                //popupToggleRatingSuccess
                function popupToggleRatingSuccess() {
                    const popupratingsuccess = document.getElementById('popupratingsuccess');
                    popupratingsuccess.classList.toggle('active');
                    indexOfRow = rowids.indexOf(selectedRowId);
                    selectedNoteId = noteids[indexOfRow];
                }

                function getRating() {
                    const btn = document.querySelector('#rateNote');
                    let selectedValue;
                    const rbs = document.querySelectorAll('input[name="rate"]');
                    for (const rb of rbs) {
                        if (rb.checked) {
                            selectedValue = rb.value;
                            if (rb.id == "star5") {
                                ratingByUser = 5;
                                //document.getElementById('star5').style("opacity", "1");
                            } else if (rb.id == "star4") {
                                ratingByUser = 4;
                                //document.getElementById('star4').style("opacity", "1");
                            } else if (rb.id == "star3") {
                                ratingByUser = 3;
                                //document.getElementById('star3').style("opacity", "1");
                            } else if (rb.id == "star2") {
                                ratingByUser = 2;
                                //document.getElementById('star2').style("opacity", "1");
                            } else if (rb.id == "star1") {
                                ratingByUser = 1;
                                //document.getElementById('starOne').style("opacity", "1");
                            }
                            else {
                                ratingByUser = 0;
                            }
                            sendNoteIDandRatingValue();
                            break;
                        }
                    }
                }
                function runPopupAndRating(rowid) {
                    popupToggleRatingSuccess();
                    getRating();
                }
            </script>

        </main>

        <!--UPLOAD POPUP-->
        <div id="popupupload">
            <div class="content">
                <p>Válaszd ki a feltölteni kívánt fájlt: </p>
                <div class="inputBox">
                    <form action="FileUploadServlet" method="post" enctype="multipart/form-data">
                        <input type="file" name="file" id="file">
                        <input type="submit" value="Feltöltés" name="upload" id="upload" class="uploadbtn" onclick="popupToggleUploadSuccess();">
                    </form>
                </div>
                <a class="closeupload" onclick="popupToggleUpload();">
                    <img src="res/close.png">
                </a>
            </div>
        </div>
        <script>
            function popupToggleUpload() {
                const popupupload = document.getElementById('popupupload');
                popupupload.classList.toggle('active');
            }
        </script>

        <!--UPLOAD SUCCESS POPUP-->
        <div id="popupuploadsuccess">
            <div class="content">
                <img src="res/ok.png">
                <h3>Sikeres feltöltés!</h3>
                <div class="inputBox">
                    <input type="submit" value="Ok" class="ratingsuccessbtn" onclick="popupToggleUploadSuccess();">
                </div>
            </div>
        </div>
        <script>
            function popupToggleUploadSuccess() {
                const popupuploadsuccess = document.getElementById('popupuploadsuccess');
                popupuploadsuccess.classList.toggle('active');
            }
        </script>

        <!--DELETE CONFIRM POPUP-->
        <div id="popupdeleteconfirm">
            <div class="content">
                <img src="res/ok.png">
                <h3>Biztosan törli a jegyzetet?</h3>
                <div class="inputBox">
                    <!-- <form method="POST" action="DeleteNote">
                    </form> -->
                    <input type="submit" value="Igen" class="deleteconfirmbtn" onclick="popupToggleDeleteConfirm();">
                    <input type="button" value="Mégse" class="deleteconfirmbtn" onclick="popupToggleDelete();">
                </div>
            </div>
        </div>
        <script>
            function popupToggleDeleteConfirm() {
                //const popupdeleteconfirm = document.getElementById('popupdeleteconfirm');
                //popupdeleteconfirm.classList.toggle('active');
                var requestToSend = {"noteId": selectedNoteId};
                console.log(selectedNoteId);
                $.ajax({
                    url: "/EgyetemiTudastar/DeleteNote",
                    type: "GET",
                    data: requestToSend,
                    success: function () {
                        delay("jegyzetek.jsp");
                        console.log("Lorem");
                    },
                    error: function () {
                        //alert("Hiba!");
                    }
                });
            }

            function getIDandDelete(element) {
                myIndex = (element.parentNode.parentNode.rowIndex) - 1;
                selectedRowId = "row" + myIndex.toString();

                indexOfRow = rowids.indexOf(selectedRowId);
                selectedNoteId = noteids[indexOfRow];

                popupToggleDelete();
            }


            function popupToggleDelete() {
                const popupdeleteconfirm = document.getElementById('popupdeleteconfirm');
                popupdeleteconfirm.classList.toggle('active');
            }
        </script>

    </body>
</html>
