package functions;

import java.sql.*;
import java.util.Objects;

public class Functions {

    /*TO DO
    TANTÁRGYHOZ TARTOZÓ JEGYZETEK KIIRATÁSA - DONE
    NOTE TÖRLÉS: userid, noteid alapján - DONE
    USER TÖRLÉS: userid alapján - DONE
    TANTÁRGY TÖRLÉS: subjectid alapján - DONE
    TANTÁRGY HOZZÁADÁS - DONE
    FELHASZNÁLÓ HOZZÁADÁSA TANTÁRGYHOZ - DONE
     */
    private static final String uname = "root";
    private static final String password = "";
    private static final String url = "jdbc:mysql://localhost:3306/tudastar";

    public static void main(String[] args) {
        //DeleteUser("ZSENAK");
        //ShowNotesBySubjectid("PTIA1-1302");
        //DeleteNotesByUserid("ZSENAK");
        //DeleteNoteByNoteid(51);
        //AddSubject("TESZTID", "TESZTNÉV");
        //AttendSubject("PTIA1-KV0201","CZIGGZ");
        //DeleteSubject("PTIA1-1302");
    }

    public static void DeleteSubject(String subjectid) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, uname, password);
            Statement stmt = (Statement) conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT COUNT(subjectid) FROM note WHERE subjectid = '" + subjectid + "'");
            Integer notes = 0;
            if (rs.next()) {
                notes = rs.getInt("COUNT(subjectid)");
            }
            if (notes > 0) {
                Integer[] noteids = new Integer[notes];
                rs = stmt.executeQuery("SELECT noteid FROM note WHERE subjectid = '" + subjectid + "'");
                Integer index = 0;
                while (rs.next()) {
                    noteids[index] = rs.getInt("noteid");
                    index++;
                }
                for (Integer i = 0; i < notes; i++) {
                    DeleteNoteByNoteid(noteids[i]);
                }
            }
            rs = stmt.executeQuery("SELECT COUNT(subjectid) FROM attendssubject WHERE subjectid = '" + subjectid + "'");
            Integer users = 0;
            if (rs.next()) {
                users = rs.getInt("COUNT(subjectid)");
            }
            rs = stmt.executeQuery("SELECT name FROM subject WHERE subjectid = '" + subjectid + "'");
            String name = "";
            if(rs.next()){
                name = rs.getString("name");
            }
            if(name != ""){
                stmt.executeUpdate("INSERT INTO deletedsubjects (subjectid, name) VALUES ('"+subjectid+"','"+name+"')");
            }
            if (users > 0) {
                String[] userids = new String[users];
                rs = stmt.executeQuery("SELECT userid FROM attendssubject WHERE subjectid = '" + subjectid + "'");
                Integer index = 0;
                while (rs.next()) {
                    userids[index] = rs.getString("userid");
                    index++;
                }
                for (Integer i = 0; i < users; i++) {
                    stmt.executeUpdate("INSERT INTO deletedas (userid, subjectid) VALUES ('" + userids[i] + "','" + subjectid + "')");
                    stmt.executeUpdate("DELETE FROM attendssubject WHERE userid = '" + userids[i] + "' AND subjectid = '" + subjectid + "'");
                }
            }
            stmt.executeUpdate("DELETE FROM subject WHERE subjectid = '" + subjectid + "'");
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void AddSubject(String subjectid, String name) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, uname, password);
            Statement stmt = (Statement) conn.createStatement();
            stmt.executeUpdate("INSERT INTO subject (subjectid, name) VALUES ('" + subjectid + "','" + name + "')");
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void AttendSubject(String subjectid, String userid) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, uname, password);
            Statement stmt = (Statement) conn.createStatement();
            stmt.executeUpdate("INSERT INTO attendssubject (subjectid, userid) VALUES ('" + subjectid + "','" + userid + "')");
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void DeleteNoteByNoteid(Integer noteid) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, uname, password);
            Statement stmt = (Statement) conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM note WHERE noteid = " + noteid);
            String subjectid = "";
            String date = "";
            String file = "";
            String notename = "";
            Boolean isNote = false;
            if (rs.next()) {
                subjectid = rs.getString("subjectid");
                date = rs.getString("uploaddate");
                file = rs.getString("file");
                notename = rs.getString("name");
                isNote = true;
            }
            if (isNote) {
                rs = stmt.executeQuery("SELECT uploaderid FROM uploadednotes WHERE noteid = " + noteid);
                String uploaderid = "TÖRÖLT FELHASZNÁLÓ";
                if (rs.next()) {
                    uploaderid = rs.getString("uploaderid");
                }
                if (uploaderid == "TÖRÖLT FELHASZNÁLÓ") {
                    rs = stmt.executeQuery("SELECT uploaderid FROM deletedun WHERE noteid = " + noteid);
                    if (rs.next()) {
                        uploaderid = rs.getString("uploaderid");
                    }
                }
                System.out.println(noteid + "|" + subjectid + "|" + date + "|" + file + "|" + notename + "|" + uploaderid);
                rs = stmt.executeQuery("SELECT COUNT(ratingid) FROM rating WHERE noteid = " + noteid);
                Integer ratings = 0;
                if (rs.next()) {
                    ratings = rs.getInt("COUNT(ratingid)");
                }
                if (ratings > 0) {
                    Integer[] ratingids = new Integer[ratings];
                    Integer[] ratingvalues = new Integer[ratings];
                    Integer index = 0;
                    rs = stmt.executeQuery("SELECT * FROM rating WHERE noteid = " + noteid);
                    while (rs.next()) {
                        ratingids[index] = rs.getInt("ratingid");
                        ratingvalues[index] = rs.getInt("value");
                        index++;
                    }
                    String[] raterids = new String[ratings];
                    for (Integer i = 0; i < ratings; i++) {
                        rs = stmt.executeQuery("SELECT raterid FROM uploadedrates WHERE ratingid = " + ratingids[i]);
                        while (rs.next()) {
                            raterids[i] = rs.getString("raterid");
                        }
                    }
                    for (Integer i = 0; i < ratings; i++) {
                        stmt.executeUpdate("INSERT INTO deletedratings (ratingid, noteid, value, raterid) VALUES (" + ratingids[i] + "," + noteid + "," + ratingvalues[i] + ",'" + raterids[i] + "')");
                        rs = stmt.executeQuery("SELECT ratingid FROM deletedur WHERE ratingid = " + ratingids[i]);
                        Boolean isRatingDeleted = false;
                        Integer rating = -1;
                        if (rs.next()) {
                            rating = rs.getInt("ratingid");
                        }
                        if (Objects.equals(rating, ratingids[i])) {
                            isRatingDeleted = true;
                        }
                        if (!isRatingDeleted) {
                            stmt.executeUpdate("INSERT INTO deletedur (ratingid, raterid) VALUES (" + ratingids[i] + ",'" + raterids[i] + "')");
                        }
                    }
                    for (Integer i = 0; i < ratings; i++) {
                        stmt.executeUpdate("DELETE FROM uploadedrates WHERE raterid = '" + raterids[i] + "' AND ratingid = " + ratingids[i]);
                    }
                    stmt.executeUpdate("DELETE FROM rating WHERE noteid = " + noteid);

                }
                rs = stmt.executeQuery("SELECT noteid FROM deletedun WHERE noteid = " + noteid);
                Boolean isInDUN = false;
                if (rs.next()) {
                    if (rs.getInt("noteid") == noteid) {
                        isInDUN = true;
                    }
                }
                if (!isInDUN) {
                    stmt.executeUpdate("INSERT INTO deletedun (noteid, uploaderid) VALUES (" + noteid + ",'" + uploaderid + "')");
                }
                stmt.executeUpdate("INSERT INTO deletednotes (noteid, uploaddate, file, name, uploaderid, subjectid) VALUES (" + noteid + ",'" + date + "','" + file + "','" + notename + "','" + uploaderid + "','" + subjectid + "')");

                stmt.executeUpdate("DELETE FROM uploadednotes WHERE noteid = " + noteid);
                stmt.executeUpdate("DELETE FROM note WHERE noteid = " + noteid);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void DeleteNotesByUserid(String userid) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, uname, password);
            Statement stmt = (Statement) conn.createStatement();
            Boolean isUserDeleted = false;
            ResultSet rs = stmt.executeQuery("SELECT userid FROM deletedusers WHERE userid = '" + userid + "'");
            String name = "";
            if (rs.next()) {
                name = rs.getString("userid");
                if (name == null ? userid == null : name.equals(userid)) {
                    isUserDeleted = true;
                }
            }

            if (isUserDeleted) {
                rs = stmt.executeQuery("SELECT COUNT(noteid) FROM deletedun WHERE uploaderid = '" + userid + "'");
                Integer notes = 0;
                if (rs.next()) {
                    notes = rs.getInt("COUNT(noteid)");
                }
                if (notes > 0) {
                    Integer[] noteids = new Integer[notes];
                    String[] dates = new String[notes];
                    String[] files = new String[notes];
                    String[] notenames = new String[notes];
                    String[] subjectids = new String[notes];
                    rs = stmt.executeQuery("SELECT noteid FROM deletedun WHERE uploaderid = '" + userid + "'");
                    Integer index = 0;
                    while (rs.next()) {
                        noteids[index] = rs.getInt("noteid");
                        index++;
                    }
                    for (Integer i = 0; i < notes; i++) {
                        rs = stmt.executeQuery("SELECT * FROM note WHERE noteid = '" + noteids[i] + "'");
                        while (rs.next()) {
                            dates[i] = rs.getString("uploaddate");
                            files[i] = rs.getString("file");
                            notenames[i] = rs.getString("name");
                            subjectids[i] = rs.getString("subjectid");
                        }
                    }
                    for (Integer i = 0; i < notes; i++) {
                        //System.out.println("[" + noteids[i] + "][" + dates[i] + "][" + files[i] + "][" + notenames[i] + "][" + subjectids[i] + "][" + userid + "]");
                        stmt.executeUpdate("INSERT INTO deletednotes (noteid, uploaddate, file, name, uploaderid, subjectid) VALUES (" + noteids[i] + ", '" + dates[i] + "', '" + files[i] + "', '" + notenames[i] + "', '" + userid + "', '" + subjectids[i] + "')");
                    }
                    Integer RATINGS = 0;
                    for (Integer i = 0; i < notes; i++) {
                        rs = stmt.executeQuery("SELECT COUNT(ratingid) FROM rating WHERE noteid =" + noteids[i]);
                        if (rs.next()) {
                            RATINGS = RATINGS + rs.getInt("Count(ratingid)");
                        }
                    }
                    Integer[] RATINGIDS = new Integer[RATINGS];
                    Integer[] RATEDNOTESIDS = new Integer[RATINGS];
                    Integer[] RATINGVALUES = new Integer[RATINGS];
                    String[] RATERIDS = new String[RATINGS];
                    if (RATINGS > 0) {
                        index = 0;
                        for (Integer i = 0; i < notes; i++) {
                            rs = stmt.executeQuery("SELECT * FROM rating WHERE noteid =" + noteids[i]);
                            while (rs.next()) {
                                RATINGIDS[index] = rs.getInt("ratingid");
                                RATEDNOTESIDS[index] = rs.getInt("noteid");
                                RATINGVALUES[index] = rs.getInt("value");
                                index++;
                            }
                        }
                        for (Integer i = 0; i < RATINGS; i++) {
                            rs = stmt.executeQuery("SELECT raterid FROM uploadedrates WHERE ratingid = " + RATINGIDS[i]);
                            while (rs.next()) {
                                RATERIDS[i] = rs.getString("raterid");
                            }
                        }
                        for (Integer i = 0; i < RATINGS; i++) {

                            stmt.executeUpdate("INSERT INTO deletedratings (ratingid, noteid, value, raterid) VALUES (" + RATINGIDS[i] + "," + RATEDNOTESIDS[i] + "," + RATINGVALUES[i] + ",'" + RATERIDS[i] + "')");
                        }
                        for (Integer i = 0; i < RATINGS; i++) {
                            stmt.executeUpdate("INSERT INTO deletedur (ratingid, raterid) VALUES (" + RATINGIDS[i] + ",'" + RATERIDS[i] + "')");
                        }
                        for (Integer i = 0; i < RATINGS; i++) {
                            stmt.executeUpdate("DELETE FROM uploadedrates WHERE raterid = '" + RATERIDS[i] + "' AND ratingid = " + RATINGIDS[i]);
                        }
                        for (Integer i = 0; i < RATINGS; i++) {
                            stmt.executeUpdate("DELETE FROM rating WHERE ratingid = '" + RATINGIDS[i] + "'");
                        }
                    }
                    for (Integer i = 0; i < notes; i++) {
                        stmt.executeUpdate("DELETE FROM note WHERE noteid = '" + noteids[i] + "'");
                    }

                }
            } else {
                rs = stmt.executeQuery("SELECT COUNT(noteid) FROM uploadednotes WHERE uploaderid = '" + userid + "'");
                Integer notes = 0;
                if (rs.next()) {
                    notes = rs.getInt("COUNT(noteid)");
                }
                if (notes > 0) {
                    Integer[] noteids = new Integer[notes];
                    String[] dates = new String[notes];
                    String[] files = new String[notes];
                    String[] notenames = new String[notes];
                    String[] subjectids = new String[notes];
                    rs = stmt.executeQuery("SELECT noteid FROM uploadednotes WHERE uploaderid = '" + userid + "'");
                    Integer index = 0;
                    while (rs.next()) {
                        noteids[index] = rs.getInt("noteid");
                        index++;
                    }
                    for (Integer i = 0; i < notes; i++) {
                        rs = stmt.executeQuery("SELECT * FROM note WHERE noteid = '" + noteids[i] + "'");
                        while (rs.next()) {
                            dates[i] = rs.getString("uploaddate");
                            files[i] = rs.getString("file");
                            notenames[i] = rs.getString("name");
                            subjectids[i] = rs.getString("subjectid");
                        }
                    }
                    for (Integer i = 0; i < notes; i++) {
                        //System.out.println("[" + noteids[i] + "][" + dates[i] + "][" + files[i] + "][" + notenames[i] + "][" + subjectids[i] + "][" + userid + "]");
                        stmt.executeUpdate("INSERT INTO deletednotes (noteid, uploaddate, file, name, uploaderid, subjectid) VALUES (" + noteids[i] + ", '" + dates[i] + "', '" + files[i] + "', '" + notenames[i] + "', '" + userid + "', '" + subjectids[i] + "')");
                        stmt.executeUpdate("INSERT INTO deletedun (noteid, uploaderid) VALUES (" + noteids[i] + ",'" + userid + "')");
                    }
                    Integer RATINGS = 0;
                    for (Integer i = 0; i < notes; i++) {
                        rs = stmt.executeQuery("SELECT COUNT(ratingid) FROM rating WHERE noteid =" + noteids[i]);
                        if (rs.next()) {
                            RATINGS = RATINGS + rs.getInt("Count(ratingid)");
                        }
                    }
                    Integer[] RATINGIDS = new Integer[RATINGS];
                    Integer[] RATEDNOTESIDS = new Integer[RATINGS];
                    Integer[] RATINGVALUES = new Integer[RATINGS];
                    String[] RATERIDS = new String[RATINGS];
                    if (RATINGS > 0) {
                        index = 0;
                        for (Integer i = 0; i < notes; i++) {
                            rs = stmt.executeQuery("SELECT * FROM rating WHERE noteid =" + noteids[i]);
                            while (rs.next()) {
                                RATINGIDS[index] = rs.getInt("ratingid");
                                RATEDNOTESIDS[index] = rs.getInt("noteid");
                                RATINGVALUES[index] = rs.getInt("value");
                                index++;
                            }
                        }
                        for (Integer i = 0; i < RATINGS; i++) {
                            rs = stmt.executeQuery("SELECT raterid FROM uploadedrates WHERE ratingid = " + RATINGIDS[i]);
                            while (rs.next()) {
                                RATERIDS[i] = rs.getString("raterid");
                            }
                        }
                        for (Integer i = 0; i < RATINGS; i++) {
                            stmt.executeUpdate("INSERT INTO deletedratings (ratingid, noteid, value, raterid) VALUES (" + RATINGIDS[i] + "," + RATEDNOTESIDS[i] + "," + RATINGVALUES[i] + ",'" + RATERIDS[i] + "')");
                            stmt.executeUpdate("INSERT INTO deletedur (ratingid, raterid) VALUES (" + RATINGIDS[i] + ",'" + RATERIDS[i] + "')");
                        }
                        for (Integer i = 0; i < RATINGS; i++) {
                            stmt.executeUpdate("DELETE FROM uploadedrates WHERE raterid = '" + RATERIDS[i] + "' AND ratingid = " + RATINGIDS[i]);
                        }
                        for (Integer i = 0; i < RATINGS; i++) {
                            stmt.executeUpdate("DELETE FROM rating WHERE ratingid = '" + RATINGIDS[i] + "'");
                        }
                    }
                    stmt.executeUpdate("DELETE FROM uploadednotes WHERE uploaderid = '" + userid + "'");
                    for (Integer i = 0; i < notes; i++) {
                        stmt.executeUpdate("DELETE FROM note WHERE noteid = '" + noteids[i] + "'");
                    }
                }
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void DeleteUser(String userid) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, uname, password);
            Statement stmt = (Statement) conn.createStatement();
            Boolean isUserDeleted = false;
            ResultSet rs = stmt.executeQuery("SELECT userid FROM deletedusers WHERE userid = '" + userid + "'");
            String name = "";
            if (rs.next()) {
                name = rs.getString("userid");
                if (name == null ? userid == null : name.equals(userid)) {
                    isUserDeleted = true;
                }
            }
            if (isUserDeleted) {
                System.out.println(userid + " már törölve van");
            } else {
                System.out.println(userid + " törlésre került");
                //DELETEDUN ELTÁROLÁS
                rs = stmt.executeQuery("SELECT COUNT(noteid) from uploadednotes WHERE uploaderid = '" + userid + "'");
                Integer notes = 0;
                if (rs.next()) {
                    notes = rs.getInt("COUNT(noteid)");
                }
                if (notes > 0) {
                    Integer[] noteids = new Integer[notes];
                    rs = stmt.executeQuery("SELECT noteid FROM uploadednotes WHERE uploaderid = '" + userid + "'");
                    Integer index = 0;
                    while (rs.next()) {
                        noteids[index] = rs.getInt("noteid");
                        index++;
                    }
                    for (Integer i = 0; i < notes; i++) {
                        stmt.executeUpdate("INSERT INTO deletedun (noteid, uploaderid) VALUES (" + noteids[i] + ",'" + userid + "')");
                        stmt.executeUpdate("DELETE FROM uploadednotes WHERE uploaderid = '" + userid + "'");
                    }
                }
                //DELETEDUR ELTÁROLÁS
                rs = stmt.executeQuery("SELECT COUNT(ratingid) from uploadedrates WHERE raterid = '" + userid + "'");
                Integer ratings = 0;
                if (rs.next()) {
                    ratings = rs.getInt("COUNT(ratingid)");
                }
                if (notes > 0) {
                    Integer[] ratingids = new Integer[ratings];
                    rs = stmt.executeQuery("SELECT ratingid FROM uploadedrates WHERE raterid = '" + userid + "'");
                    Integer index = 0;
                    while (rs.next()) {
                        ratingids[index] = rs.getInt("ratingid");
                        index++;
                    }
                    for (Integer i = 0; i < ratings; i++) {
                        stmt.executeUpdate("INSERT INTO deletedur (ratingid, raterid) VALUES (" + ratingids[i] + ",'" + userid + "')");
                        stmt.executeUpdate("DELETE FROM uploadedrates WHERE raterid = '" + userid + "'");
                    }
                }
                //DELETEDAS ELTÁROLÁS
                rs = stmt.executeQuery("SELECT COUNT(userid) from attendssubject WHERE userid = '" + userid + "'");
                Integer subjects = 0;
                if (rs.next()) {
                    subjects = rs.getInt("COUNT(userid)");
                }
                if (subjects > 0) {
                    String[] subjectids = new String[subjects];
                    rs = stmt.executeQuery("SELECT subjectid FROM attendssubject WHERE userid = '" + userid + "'");
                    Integer index = 0;
                    while (rs.next()) {
                        subjectids[index] = rs.getString("subjectid");
                        index++;
                    }
                    for (Integer i = 0; i < subjects; i++) {
                        stmt.executeUpdate("INSERT INTO deletedas (userid, subjectid) VALUES ('" + userid + "','" + subjectids[i] + "')");
                        stmt.executeUpdate("DELETE FROM attendssubject WHERE userid = '" + userid + "'");
                    }
                }
                //EDUCOIN LEKÉRDEZÉS ÉS USER TÖRLÉS
                rs = stmt.executeQuery("SELECT * FROM user WHERE userid = '" + userid + "'");
                String email = "";
                String firstname = "";
                String lastname = "";
                String password = "";
                if (rs.next()) {
                    email = rs.getString("email");
                    firstname = rs.getString("firstname");
                    lastname = rs.getString("lastname");
                    password = rs.getString("password");
                }
                rs = stmt.executeQuery("SELECT quantity FROM educoin WHERE ownerid = '" + userid + "'");
                Integer educoin = 0;
                if (rs.next()) {
                    educoin = rs.getInt("quantity");
                }
                stmt.executeUpdate("INSERT INTO deletedusers (userid, email, firstname, lastname, password, educoin) VALUES ('" + userid + "','" + email + "','" + firstname + "','" + lastname + "','" + password + "'," + educoin + ")");
                stmt.executeUpdate("DELETE FROM educoin WHERE ownerid = '" + userid + "'");
                stmt.executeUpdate("DELETE FROM user WHERE userid = '" + userid + "'");
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void ShowNotesBySubjectid(String subjectid) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, uname, password);
            Statement stmt = (Statement) conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM note WHERE subjectid = '" + subjectid + "'");
            Integer notes = 0;
            if (rs.next()) {
                notes = rs.getInt("COUNT(*)");
            }
            rs = stmt.executeQuery("SELECT noteid FROM note WHERE subjectid = '" + subjectid + "'");
            Integer[] noteids = new Integer[notes];
            Integer index = 0;
            while (rs.next()) {
                noteids[index] = rs.getInt("noteid");
                index++;
            }
            String[] names = new String[notes];
            index = 0;
            for (Integer i = 0; i < notes; i++) {
                rs = stmt.executeQuery("SELECT uploaderid,noteid FROM uploadednotes WHERE noteid =" + noteids[i]);
                while (rs.next()) {
                    for (Integer j = 0; j < notes; j++) {
                        if (noteids[j] == rs.getInt("noteid")) {
                            names[j] = rs.getString("uploaderid");
                            index++;
                        }
                    }
                }
            }
            for (Integer i = 0; i < notes; i++) {
                if (names[i] == null) {
                    names[i] = "TÖRÖLT FELHASZNÁLÓ";
                }
            }
            for (Integer i = 0; i < notes; i++) {
                rs = stmt.executeQuery("SELECT note.noteid, name, file, uploaderid, subjectid FROM note, uploadednotes WHERE uploadednotes.noteid = " + noteids[i] + " AND note.noteid = " + noteids[i]);
                if (rs.next()) {
                    System.out.println(rs.getString("name") + " | " + rs.getString("file") + " | " + rs.getString("uploaderid") + " | " + rs.getString("subjectid") + " | " + rs.getString("noteid"));
                }
            }
            for (Integer i = 0; i < notes; i++) {
                rs = stmt.executeQuery("SELECT note.noteid, name, file, subjectid FROM note WHERE noteid = " + noteids[i]);
                if (rs.next()) {
                    if (names[i] == "TÖRÖLT FELHASZNÁLÓ") {
                        System.out.println(rs.getString("name") + " | " + rs.getString("file") + " | " + "TÖRÖLT FELHASZNÁLÓ" + " | " + rs.getString("subjectid") + " | " + rs.getString("noteid"));
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println(e);
        }
    }
}
