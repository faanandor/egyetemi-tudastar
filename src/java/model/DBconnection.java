package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public final class DBconnection {
    static Connection con  = null;

    private DBconnection(){
    }
    
    public static Connection getDBConnection() {
        String url = "jdbc:mysql://localhost:3307/tudastar";
        String uname = "root";
        String password = "";
        try {
            if(con == null){
                Class.forName("com.mysql.jdbc.Driver");
                Properties props = new Properties();
                con = DriverManager.getConnection(url, uname, password);
            }
        }
        catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        
        return con;
    }
}