package model;

import java.sql.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import service.EgyetemiTudastarService;
import static controller.EgyetemiTudastarController.globalNeptunCode;
import java.security.NoSuchAlgorithmException;
import java.time.format.DateTimeFormatter;
import org.json.JSONException;

public class EgyetemiTudastarModel {
    public static boolean globalErteklehet;
    
    public static Boolean login(String neptun, String pword) throws IOException, NoSuchAlgorithmException {
        String query = "select * from user";
        Boolean result = Boolean.FALSE;

        try {
            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                String hashedPword = EgyetemiTudastarService.generateHash(pword, "MD5");
                if (neptun.equals(rs.getString("userid")) && hashedPword.equals((rs.getString("password")))) {
                    result = Boolean.TRUE;
                    break;
                } else {
                    result = Boolean.FALSE;
                }
            }

        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return result;
    }

    public static Boolean isTeacher(String neptun) throws IOException {
        String query = "select * from user";
        Boolean isTeacher = Boolean.FALSE;

        try {
            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                if (neptun.equals(rs.getString("userid")) && Integer.parseInt(rs.getString("isTeacher")) == 1) {
                    isTeacher = Boolean.TRUE;
                    break;
                } else {
                    isTeacher = Boolean.FALSE;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return isTeacher;
    }

    public static Boolean isAdmin(String neptun, String pword) throws IOException, NoSuchAlgorithmException {
        String query = "select * from admin";
        Boolean idAdmin = Boolean.FALSE;
        try {
            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            String hashedPword = EgyetemiTudastarService.generateHash(pword, "MD5");
            while (rs.next()) {
                if (neptun.equals(rs.getString("name")) && hashedPword.equals((rs.getString("password")))) {
                    idAdmin = Boolean.TRUE;
                    break;
                } else {
                    idAdmin = Boolean.FALSE;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return idAdmin;
    }

    public static List<String> getAllData(String neptun) throws IOException {
        List<String> data = new ArrayList<String>();
        String query = "select * from user";

        try {
            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                if (neptun.equals(rs.getString("userid"))) {
                    String name = rs.getString("firstname") + " " + rs.getString("lastname"); // ?sszerakjuk k??t mez?b?l a teljes nevet
                    data.add(name);
                    data.add(neptun);
                    break;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return data;
    }

    public static Map<String, String> getSubjects(String neptun) throws IOException {
        List<String> subjects = new ArrayList<String>();
        List<String> subjectid = new ArrayList<String>();
        Map<String, String> map = new HashMap<String, String>();

        String query = "select subject.name, subject.subjectid\n"
                + "from user, subject, attendssubject\n"
                + "where user.userid = attendssubject.userid\n"
                + "and subject.subjectid = attendssubject.subjectid\n"
                + "and user.userid =" + "'" + neptun + "'";

        try {
            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            //System.out.println("Felvett t�rgyak: ");
            while (rs.next()) {
                subjects.add(rs.getString("name"));
                subjectid.add(rs.getString("subjectid"));
                map.put(rs.getString("name"), rs.getString("subjectid"));
                //System.out.println(rs.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }

        return map;
    }

    public static Map<String, String> getApplySubjects(String neptun) throws IOException {
        List<String> subjects = new ArrayList<String>();
        List<String> subjectid = new ArrayList<String>();
        Map<String, String> map = new HashMap<String, String>();
        Map<String, Boolean> mapFlag = new HashMap<String, Boolean>();
        Map<String, String> mapExist = getSubjects(neptun);

        String query = "SELECT subjectid, name\n"
                + "FROM subject \n"
                + "where subjectid not in(SELECT subjectid FROM attendssubject where userid = '" + neptun + "');";

        try {
            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            System.out.println("");
            System.out.println("");
            System.out.println("�sszes t�rgy");
            while (rs.next()) {
                //System.out.println(rs.getString("name"));
                map.put(rs.getString("name"), rs.getString("subjectid"));
            }

        } catch (SQLException e) {
            System.out.println(e.toString());
        }

        return map;
    }

    public static Map<String, String> getSubjectsAdmin() throws IOException {
        List<String> subjects = new ArrayList<String>();
        List<String> subjectid = new ArrayList<String>();
        Map<String, String> map = new HashMap<String, String>();

        String query = "SELECT * FROM subject";

        try {
            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                subjects.add(rs.getString("name"));
                subjectid.add(rs.getString("subjectid"));
                map.put(rs.getString("name"), rs.getString("subjectid"));
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }

        return map;
    }

    public static List<String> getEduCoin(String neptun) throws IOException {
        List<String> edu = new ArrayList<String>();

        String query = "select educoin.quantity, levels.name\n"
                + "from user, educoin, levels\n"
                + "where user.userid = educoin.ownerid\n"
                + "and educoin.level = levels.level\n"
                + "and user.userid =" + "'" + neptun + "'";

        try {
            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                edu.add(rs.getString("name"));
                edu.add(rs.getString("quantity").toString());
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }

        return edu;
    }

    public static JSONArray getEduCoinShopData() throws IOException, JSONException {
        JSONArray shopData = new JSONArray();

        try {
            String query = "SELECT path, price, name FROM `reward` ORDER BY `rewardid` DESC";
            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                JSONObject obj = new JSONObject();
                obj.put("path", rs.getString("path"));
                obj.put("price", rs.getString("price"));
                obj.put("name", rs.getString("name"));
                shopData.put(obj);
            }

        } catch (SQLException e) {
            System.out.println(e.toString());
        }

        return shopData;
    }

    public static long getNoteRating(String noteId) throws IOException {
        Double ratingSum = 0.0;
        Double ratingDb = 0.0;
        Double ratingAvg = 0.0;
        try {
            String query = "SELECT noteid, value FROM rating WHERE noteid = " + noteId;
            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                ratingSum += Integer.parseInt(rs.getString("value"));
                ratingDb++;
            }

            ratingAvg = ratingSum / ratingDb;
            //System.out.println(Math.round(ratingAvg));
        } catch (Exception e) {
            System.out.println(e);
        }
        return Math.round(ratingAvg);
    }

    public static JSONArray getSubjectNotes(String desiredSubjectId) throws IOException {
        List<Integer> noteId = new ArrayList<Integer>();
        List<String> noteName = new ArrayList<String>();
        List<String> subjectid = new ArrayList<String>();
        List<String> uploaddate = new ArrayList<String>();
        List<String> file = new ArrayList<String>();
        List<String> userid = new ArrayList<String>();
        List<String> isteacher = new ArrayList<String>();
        List<Long> ratingValue = new ArrayList<Long>();

        String query = "SELECT note.name, note.noteid, note.subjectid, note.uploaddate, note.file, user.userid, user.isteacher, uploadednotes.uploaderid\n"
                + "FROM user, note, uploadednotes, uploadedrates\n"
                + "WHERE note.subjectid = '" + desiredSubjectId + "' AND note.noteid = uploadednotes.noteid AND user.userid = uploadednotes.uploaderid\n"
                + "GROUP BY note.noteid\n"
                + "ORDER BY user.isteacher DESC;";

        try {
            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                noteId.add(Integer.parseInt(rs.getString("noteid")));
                noteName.add(rs.getString("name"));
                subjectid.add(rs.getString("subjectid"));
                uploaddate.add(rs.getString("uploaddate"));
                file.add(rs.getString("file"));
                userid.add(rs.getString("userid"));
                isteacher.add(rs.getString("isteacher"));
                ratingValue.add(getNoteRating(rs.getString("noteid")));
            }

            //T�r�lt felhaszn�l� jegyzeteinek hozz�ad�sa
            query = "SELECT note.name, note.noteid, note.subjectid, note.uploaddate, note.file, deletedusers.userid, deletedusers.isteacher, deletedun.uploaderid\n"
                    + "FROM deletedusers, note, deletedun, uploadedrates\n"
                    + "WHERE note.subjectid = '" + desiredSubjectId + "' AND \n"
                    + "note.noteid = deletedun.noteid AND \n"
                    + "deletedusers.userid = deletedun.uploaderid\n"
                    + "GROUP BY note.noteid\n"
                    + "ORDER BY deletedusers.isteacher DESC;";
            rs = st.executeQuery(query);

            while (rs.next()) {
                noteId.add(Integer.parseInt(rs.getString("noteid")));
                noteName.add(rs.getString("name"));
                subjectid.add(rs.getString("subjectid"));
                uploaddate.add(rs.getString("uploaddate"));
                file.add(rs.getString("file"));
                userid.add("T�r�lt felhaszn�l�");
                isteacher.add(rs.getString("isteacher"));
                ratingValue.add(getNoteRating(rs.getString("noteid")));
            }

        } catch (SQLException e) {
            System.out.println(e.toString());
        }

        JSONArray allData = new JSONArray();

        try {
            for (int i = 0; i < noteName.size(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("noteid", noteId.get(i));
                obj.put("name", noteName.get(i));
                obj.put("subjectid", subjectid.get(i));
                obj.put("uploaddate", uploaddate.get(i));
                obj.put("file", file.get(i));
                obj.put("userid", userid.get(i));
                if (isteacher.get(i).equals("1")) {
                    obj.put("isTeacher", "tan�r");
                } else {
                    obj.put("isTeacher", "di�k");
                }

                obj.put("ratingValue", ratingValue.get(i));
                allData.put(obj);
                //System.out.println(noteName.get(i));

            }

        } catch (Exception ex) {
            System.out.println("Hali " + ex.toString());
            System.out.println(ex.getStackTrace()[0]);
            System.out.println(ex.getCause());
            ex.printStackTrace();
        }

        return allData;
    }

    public static void storeNoteRating(String noteId, String ratingValue, String raterNeptunCode) throws NumberFormatException, IOException {
        // Rating t�bla friss�t�se

        try {
            String query = "SELECT rating.noteid, uploadedrates.raterid\n"
                    + "FROM rating, uploadedrates\n"
                    + "WHERE uploadedrates.raterid = '" + raterNeptunCode + "' and uploadedrates.ratingid = rating.ratingid;";

            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            // TODO: Ellen�rizz�k, hogy a user �rt�kelte-e m�r a jegyzetet
            ResultSet rs = st.executeQuery(query);
            List<String> storedRatersids = new ArrayList<String>();
            List<String> storedNoteids = new ArrayList<String>();

            while (rs.next()) {
                String neptun = rs.getString("raterid");
                String noteAzon = rs.getString("noteid");
                storedRatersids.add(neptun);
                storedNoteids.add(noteAzon);
            }

            boolean ertekelhet = Boolean.TRUE;

            for (int i = 0; i < storedRatersids.size(); i++) {
                if ((storedRatersids.get(i).equals(raterNeptunCode)) && (storedNoteids.get(i).equals(noteId))) {
                    //TODO: fix this
                    ertekelhet = Boolean.FALSE;
                    break;
                } else {
                    ertekelhet = Boolean.TRUE;
                }
            }
            globalErteklehet = ertekelhet;
            System.out.println("Mostani global value: " + globalErteklehet);
            System.out.println("Ertekelhet Model: " + ertekelhet);

            if (ertekelhet) {
                query = "INSERT INTO rating (rating.noteid, rating.value)\n"
                        + "VALUES ( " + noteId + "," + ratingValue + ");";

                st.executeUpdate(query);
                // Uploadedrates t�bla friss�t�se
                query = "SELECT ratingid FROM rating WHERE rating.noteid = " + noteId + " AND rating.value = " + ratingValue + ";";
                rs = st.executeQuery(query);
                Integer wantedRatingId = 0;
                while (rs.next()) {
                    wantedRatingId = Integer.parseInt(rs.getString("ratingid"));
                }

                query = "INSERT INTO uploadedrates (uploadedrates.ratingid, uploadedrates.raterid)\n"
                        + "VALUES ( " + wantedRatingId + ", '" + globalNeptunCode + "');";
                st.executeUpdate(query);

                //Update Educoin
                query = "SELECT uploadednotes.uploaderid\n"
                        + "FROM uploadednotes\n"
                        + "WHERE uploadednotes.noteid = " + noteId;

                st = con.createStatement();
                rs = st.executeQuery(query);

                String uploaderid = "";

                while (rs.next()) {
                    uploaderid = rs.getString("uploaderid");
                }
                Integer add = Integer.parseInt(ratingValue);
                if (add >= 3) {
                    if (isTeacher(raterNeptunCode)) {
                        add *= 2;
                    }
                    query = "UPDATE educoin SET quantity = quantity + " + add + "\n"
                            + "WHERE educoin.ownerid = '" + uploaderid + "'; ";
                    st.executeUpdate(query);
                }

                //Szint friss�t�se
                levelUpdate(uploaderid, con);
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }

    public static void insertNewNote(String subjectid, String uploaddate, String fileType, String noteName) {
        String query = "INSERT INTO note (note.subjectid, note.uploaddate, note.file, note.name)\n"
                + "VALUES ('" + subjectid + "' ,'" + uploaddate + "', '" + fileType + "', '" + noteName + "');";

        try {
            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            st.executeUpdate(query);

            //System.out.println(noteName);
            query = "SELECT noteid FROM note WHERE note.subjectid = '" + subjectid + "' AND note.uploaddate = '" + uploaddate + "'\n"
                    + "AND note.file = '" + fileType + "' AND note.name = '" + noteName + "';";
            ResultSet rs = st.executeQuery(query);
            Integer wantedNoteId = 0;
            while (rs.next()) {
                wantedNoteId = Integer.parseInt(rs.getString("noteid"));
            }

            System.out.println("Keresett: " + wantedNoteId);

            query = "INSERT INTO uploadednotes (noteid, uploaderid) VALUES( " + wantedNoteId + ", '" + globalNeptunCode + "');";
            st.executeUpdate(query);

            int amount = 3;
            query = "UPDATE educoin SET quantity = quantity + " + amount + "\n"
                    + "WHERE educoin.ownerid = '" + globalNeptunCode + "'; ";
            st.executeUpdate(query);

            //Szint friss�t�se
            levelUpdate(globalNeptunCode, con);

        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }

    public static void DeleteNoteByNoteid(Integer noteid) {
        Connection conn = null;
        try {
            conn = DBconnection.getDBConnection();
            Statement stmt = (Statement) conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM note WHERE noteid = " + noteid);
            String subjectid = "";
            String date = "";
            String file = "";
            String notename = "";
            Boolean isNote = false;
            if (rs.next()) {
                subjectid = rs.getString("subjectid");
                date = rs.getString("uploaddate");
                file = rs.getString("file");
                notename = rs.getString("name");
                isNote = true;
            }
            if (isNote) {
                rs = stmt.executeQuery("SELECT uploaderid FROM uploadednotes WHERE noteid = " + noteid);
                String uploaderid = "T�R�LT FELHASZN�L�";
                if (rs.next()) {
                    uploaderid = rs.getString("uploaderid");
                }
                if (uploaderid == "T�R�LT FELHASZN�L�") {
                    rs = stmt.executeQuery("SELECT uploaderid FROM deletedun WHERE noteid = " + noteid);
                    if (rs.next()) {
                        uploaderid = rs.getString("uploaderid");
                    }
                }
                System.out.println(noteid + "|" + subjectid + "|" + date + "|" + file + "|" + notename + "|" + uploaderid);
                rs = stmt.executeQuery("SELECT COUNT(ratingid) FROM rating WHERE noteid = " + noteid);
                Integer ratings = 0;
                if (rs.next()) {
                    ratings = rs.getInt("COUNT(ratingid)");
                }
                if (ratings > 0) {
                    Integer[] ratingids = new Integer[ratings];
                    Integer[] ratingvalues = new Integer[ratings];
                    Integer index = 0;
                    rs = stmt.executeQuery("SELECT * FROM rating WHERE noteid = " + noteid);
                    while (rs.next()) {
                        ratingids[index] = rs.getInt("ratingid");
                        ratingvalues[index] = rs.getInt("value");
                        index++;
                    }
                    String[] raterids = new String[ratings];
                    for (Integer i = 0; i < ratings; i++) {
                        rs = stmt.executeQuery("SELECT raterid FROM uploadedrates WHERE ratingid = " + ratingids[i]);
                        while (rs.next()) {
                            raterids[i] = rs.getString("raterid");
                        }
                    }
                    for (Integer i = 0; i < ratings; i++) {
                        stmt.executeUpdate("INSERT INTO deletedratings (ratingid, noteid, value, raterid) VALUES (" + ratingids[i] + "," + noteid + "," + ratingvalues[i] + ",'" + raterids[i] + "')");
                        stmt.executeUpdate("INSERT INTO deletedur (ratingid, raterid) VALUES (" + ratingids[i] + ",'" + raterids[i] + "')");
                    }
                    for (Integer i = 0; i < ratings; i++) {
                        stmt.executeUpdate("DELETE FROM uploadedrates WHERE raterid = '" + raterids[i] + "' AND ratingid = " + ratingids[i]);
                    }
                    stmt.executeUpdate("DELETE FROM rating WHERE noteid = " + noteid);

                }
                rs = stmt.executeQuery("SELECT noteid FROM deletedun WHERE noteid = " + noteid);
                Boolean isInDUN = false;
                if (rs.next()) {
                    if (rs.getInt("noteid") == noteid) {
                        isInDUN = true;
                    }
                }
                if (!isInDUN) {
                    stmt.executeUpdate("INSERT INTO deletedun (noteid, uploaderid) VALUES (" + noteid + ",'" + uploaderid + "')");
                }
                stmt.executeUpdate("INSERT INTO deletednotes (noteid, uploaddate, file, name, uploaderid, subjectid) VALUES (" + noteid + ",'" + date + "','" + file + "','" + notename + "','" + uploaderid + "','" + subjectid + "')");

                stmt.executeUpdate("DELETE FROM uploadednotes WHERE noteid = " + noteid);
                stmt.executeUpdate("DELETE FROM note WHERE noteid = " + noteid);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void DeleteSubject(String subjectid) {
        Connection conn = null;
        try {
            conn = DBconnection.getDBConnection();
            Statement stmt = (Statement) conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT COUNT(subjectid) FROM note WHERE subjectid = '" + subjectid + "'");
            Integer notes = 0;
            if (rs.next()) {
                notes = rs.getInt("COUNT(subjectid)");
            }
            if (notes > 0) {
                Integer[] noteids = new Integer[notes];
                rs = stmt.executeQuery("SELECT noteid FROM note WHERE subjectid = '" + subjectid + "'");
                Integer index = 0;
                while (rs.next()) {
                    noteids[index] = rs.getInt("noteid");
                    index++;
                }
                for (Integer i = 0; i < notes; i++) {
                    DeleteNoteByNoteid(noteids[i]);
                }
            }
            rs = stmt.executeQuery("SELECT COUNT(subjectid) FROM attendssubject WHERE subjectid = '" + subjectid + "'");
            Integer users = 0;
            if (rs.next()) {
                users = rs.getInt("COUNT(subjectid)");
            }
            rs = stmt.executeQuery("SELECT name FROM subject WHERE subjectid = '" + subjectid + "'");
            String name = "";
            if (rs.next()) {
                name = rs.getString("name");
            }
            if (name != "") {
                stmt.executeUpdate("INSERT INTO deletedsubjects (subjectid, name) VALUES ('" + subjectid + "','" + name + "')");
            }
            if (users > 0) {
                String[] userids = new String[users];
                rs = stmt.executeQuery("SELECT userid FROM attendssubject WHERE subjectid = '" + subjectid + "'");
                Integer index = 0;
                while (rs.next()) {
                    userids[index] = rs.getString("userid");
                    index++;
                }
                for (Integer i = 0; i < users; i++) {
                    stmt.executeUpdate("INSERT INTO deletedas (userid, subjectid) VALUES ('" + userids[i] + "','" + subjectid + "')");
                    stmt.executeUpdate("DELETE FROM attendssubject WHERE userid = '" + userids[i] + "' AND subjectid = '" + subjectid + "'");
                }
            }
            stmt.executeUpdate("DELETE FROM subject WHERE subjectid = '" + subjectid + "'");
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static JSONArray getUsersAdmin() throws IOException, JSONException {
        String query = "SELECT firstname, lastname, userid, educoin.quantity, levels.name\n"
                + "FROM user, educoin, levels\n"
                + "WHERE user.userid = educoin.ownerid AND\n"
                + "educoin.level = levels.level  \n"
                + "ORDER BY `educoin`.`quantity`  DESC;";

        JSONArray allData = new JSONArray();
        try {
            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                JSONObject obj = new JSONObject();
                obj.put("userName", rs.getString("firstname") + " " + rs.getString("lastname"));
                obj.put("neptunCode", rs.getString("userid"));
                obj.put("educoin", rs.getString("quantity"));
                obj.put("level", rs.getString("name"));

                //System.out.println(obj.get("userName") + " - " + obj.get("neptunCode") + " - " + obj.get("educoin") + " - " + obj.get("level"));
                allData.put(obj);
            }

        } catch (SQLException e) {
            System.out.println(e.toString());
        }

        return allData;
    }

    public static void DeleteUser(String userid) {
        Connection conn = null;
        try {
            conn = DBconnection.getDBConnection();
            Statement stmt = (Statement) conn.createStatement();
            Boolean isUserDeleted = false;
            ResultSet rs = stmt.executeQuery("SELECT userid FROM deletedusers WHERE userid = '" + userid + "'");
            String name = "";
            if (rs.next()) {
                name = rs.getString("userid");
                if (name == null ? userid == null : name.equals(userid)) {
                    isUserDeleted = true;
                }
            }
            if (isUserDeleted) {
            } else {
                //DELETEDUN ELT�ROL�S
                rs = stmt.executeQuery("SELECT COUNT(noteid) from uploadednotes WHERE uploaderid = '" + userid + "'");
                Integer notes = 0;
                if (rs.next()) {
                    notes = rs.getInt("COUNT(noteid)");
                }
                if (notes > 0) {
                    Integer[] noteids = new Integer[notes];
                    rs = stmt.executeQuery("SELECT noteid FROM uploadednotes WHERE uploaderid = '" + userid + "'");
                    Integer index = 0;
                    while (rs.next()) {
                        noteids[index] = rs.getInt("noteid");
                        index++;
                    }
                    for (Integer i = 0; i < notes; i++) {
                        stmt.executeUpdate("INSERT INTO deletedun (noteid, uploaderid) VALUES (" + noteids[i] + ",'" + userid + "')");
                        stmt.executeUpdate("DELETE FROM uploadednotes WHERE uploaderid = '" + userid + "'");
                    }
                }
                //DELETEDUR ELT?ELT�ROL�S
                rs = stmt.executeQuery("SELECT COUNT(ratingid) from uploadedrates WHERE raterid = '" + userid + "'");
                Integer ratings = 0;
                if (rs.next()) {
                    ratings = rs.getInt("COUNT(ratingid)");
                }
                if (ratings > 0) {
                    Integer[] ratingids = new Integer[ratings];
                    rs = stmt.executeQuery("SELECT ratingid FROM uploadedrates WHERE raterid = '" + userid + "'");
                    Integer index = 0;
                    while (rs.next()) {
                        ratingids[index] = rs.getInt("ratingid");
                        index++;
                    }
                    for (Integer i = 0; i < ratings; i++) {
                        stmt.executeUpdate("INSERT INTO deletedur (ratingid, raterid) VALUES (" + ratingids[i] + ",'" + userid + "')");
                        stmt.executeUpdate("DELETE FROM uploadedrates WHERE raterid = '" + userid + "'");
                    }
                }
                //DELETEDAS ELT�ROL�S
                rs = stmt.executeQuery("SELECT COUNT(userid) from attendssubject WHERE userid = '" + userid + "'");
                Integer subjects = 0;
                if (rs.next()) {
                    subjects = rs.getInt("COUNT(userid)");
                }
                if (subjects > 0) {
                    String[] subjectids = new String[subjects];
                    rs = stmt.executeQuery("SELECT subjectid FROM attendssubject WHERE userid = '" + userid + "'");
                    Integer index = 0;
                    while (rs.next()) {
                        subjectids[index] = rs.getString("subjectid");
                        index++;
                    }
                    for (Integer i = 0; i < subjects; i++) {
                        stmt.executeUpdate("INSERT INTO deletedas (userid, subjectid) VALUES ('" + userid + "','" + subjectids[i] + "')");
                        stmt.executeUpdate("DELETE FROM attendssubject WHERE userid = '" + userid + "'");
                    }
                }
                //EDUCOIN LEK�RDEZ�S �S USER T�ROL�S
                rs = stmt.executeQuery("SELECT * FROM user WHERE userid = '" + userid + "'");
                String email = "";
                String firstname = "";
                String lastname = "";
                String password = "";
                Integer isTeacher = 0;
                if (rs.next()) {
                    email = rs.getString("email");
                    firstname = rs.getString("firstname");
                    lastname = rs.getString("lastname");
                    password = rs.getString("password");
                    isTeacher = rs.getInt("isteacher");
                }
                rs = stmt.executeQuery("SELECT quantity FROM educoin WHERE ownerid = '" + userid + "'");
                Integer educoin = 0;
                if (rs.next()) {
                    educoin = rs.getInt("quantity");
                }
                stmt.executeUpdate("INSERT INTO deletedusers (userid, email, firstname, lastname, password, educoin, isteacher) VALUES ('" + userid + "','" + email + "','" + firstname + "','" + lastname + "','" + password + "'," + educoin + "," + isTeacher + ")");
                stmt.executeUpdate("DELETE FROM educoin WHERE ownerid = '" + userid + "'");
                stmt.executeUpdate("DELETE FROM user WHERE userid = '" + userid + "'");
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void AddSubject(String subjectid, String name) {
        Connection conn = null;
        try {
            conn = DBconnection.getDBConnection();
            Statement stmt = (Statement) conn.createStatement();
            stmt.executeUpdate("INSERT INTO subject (subjectid, name) VALUES ('" + subjectid + "','" + name + "')");
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void AttendSubject(String subjectid, String userid) {
        Connection conn = null;
        try {
            conn = DBconnection.getDBConnection();
            Statement stmt = (Statement) conn.createStatement();
            stmt.executeUpdate("INSERT INTO attendssubject (subjectid, userid) VALUES ('" + subjectid + "','" + userid + "')");
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void DecreaseEducoin(String amount) {
        Connection conn = null;
        try {
            conn = DBconnection.getDBConnection();
            Statement st = (Statement) conn.createStatement();

            String query = "UPDATE educoin SET quantity = quantity - " + amount + " \n"
                    + "WHERE educoin.ownerid = '" + globalNeptunCode + "';";

            st.executeUpdate(query);

            //Szint friss�t�se
            levelUpdate(globalNeptunCode, conn);

            query = "select rewardid from reward where price = " + amount + ";";
            ResultSet rs = st.executeQuery(query);

            Integer itemID = 0;
            while (rs.next()) {
                itemID = rs.getInt("rewardid");
            }
            //System.out.println("Termek : " + itemName);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String uploaddate = dtf.format(java.time.LocalDateTime.now());

            st.executeUpdate("INSERT INTO transactions (userid, rewardid, date) VALUES ('" + globalNeptunCode + "', " + itemID + ", '" + uploaddate + "');");
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    // Megn�zz�k, hogy a user �rt�kelte-e a jegyzetet
    public static boolean alreadyRated(String raterNeptunCode, String noteId) {
        boolean ertekelhet = Boolean.TRUE;
        try {
            String query = "SELECT rating.noteid, uploadedrates.raterid\n"
                    + "FROM rating, uploadedrates\n"
                    + "WHERE uploadedrates.raterid = '" + raterNeptunCode + "' and uploadedrates.ratingid = rating.ratingid;";

            Connection con = DBconnection.getDBConnection();
            Statement st = con.createStatement();
            // TODO: Ellen�rizz�k, hogy a user �rt�kelte-e m�r a jegyzetet
            ResultSet rs = st.executeQuery(query);
            List<String> storedRatersids = new ArrayList<String>();
            List<String> storedNoteids = new ArrayList<String>();

            while (rs.next()) {
                String neptun = rs.getString("raterid");
                String noteAzon = rs.getString("noteid");
                storedRatersids.add(neptun);
                storedNoteids.add(noteAzon);
            }


            for (int i = 0; i < storedRatersids.size(); i++) {
                if ((storedRatersids.get(i).equals(raterNeptunCode)) && (storedNoteids.get(i).equals(noteId))) {
                    //TODO: fix this
                    ertekelhet = Boolean.FALSE;
                    break;
                } else {
                    ertekelhet = Boolean.TRUE;
                }
            }
        }
        catch (SQLException e) {
            System.out.println(e.toString());
        }
        return ertekelhet;
    }

    //Szint friss�t�se
    public static void levelUpdate(String raterNeptunCode, Connection conn) {
        try {
            int level = 0;
            int quantity = 0;
            String query = "SELECT quantity FROM `educoin` where ownerid = '" + raterNeptunCode + "';";
            Statement st = (Statement) conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                quantity = Integer.parseInt(rs.getString("quantity"));
            }

            if (500 <= quantity) //Kir�ly szint
            {
                query = "UPDATE `educoin` SET level= 4 WHERE ownerid = '" + raterNeptunCode + "';";
                st.executeUpdate(query);
            }

            if (400 <= quantity && quantity < 500) //Herceg szint
            {
                query = "UPDATE `educoin` SET level= 3 WHERE ownerid = '" + raterNeptunCode + "';";
                st.executeUpdate(query);
            }

            if (250 <= quantity && quantity < 400) //B�r� szint
            {
                query = "UPDATE `educoin` SET level= 2 WHERE ownerid = '" + raterNeptunCode + "';";
                st.executeUpdate(query);
            }

            if (100 <= quantity && quantity < 250) //Lovag szint
            {
                query = "UPDATE `educoin` SET level= 1 WHERE ownerid = '" + raterNeptunCode + "';";
                st.executeUpdate(query);
            }
            if (100 > quantity) //Nincs szint
            {
                query = "UPDATE `educoin` SET level= 0 WHERE ownerid = '" + raterNeptunCode + "';";
                st.executeUpdate(query);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
