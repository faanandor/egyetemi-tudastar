package controller;

import static controller.EgyetemiTudastarController.globalNeptunCode;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.EgyetemiTudastarModel;

@WebServlet(name = "AttendSubject", urlPatterns = {"/AttendSubject"})
public class AttendSubject extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String subjectid = request.getParameter("subjectId");
        String userid = globalNeptunCode;
        if ((!userid.isEmpty()) && (!subjectid.isEmpty())) {
            EgyetemiTudastarModel.AttendSubject(subjectid, userid);
        } else {
            //request.getRequestDispatcher("targyak.html").include(request, response);
            // getServletContext().getRequestDispatcher("/targyak.html").forward (request, response);
        }
    }
}
