package controller;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.EgyetemiTudastarModel;

@WebServlet(name = "AddSubject", urlPatterns = {"/AddSubject"})
public class AddSubject extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String subjectid = request.getParameter("subId");
        String subjectName = request.getParameter("subName");
        if ((!subjectName.isEmpty()) && (!subjectid.isEmpty())) {
            EgyetemiTudastarModel.AddSubject(subjectid, subjectName);
        } else {
            //request.getRequestDispatcher("targyak.html").include(request, response);
            // getServletContext().getRequestDispatcher("/targyak.html").forward (request, response);
            
        }
    }
}
