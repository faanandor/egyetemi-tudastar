package controller;

import static controller.EgyetemiTudastarController.globalNeptunCode;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.EgyetemiTudastarModel;
import org.json.JSONArray;
import org.json.JSONObject;

@WebServlet(name = "LoadData", urlPatterns = {"/LoadData"})
public class LoadData extends HttpServlet {

    public static String globalNoteId = "";
    public static String globalSubjectId = "";

    public static void processDataRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        try {
            if (request.getParameter("task") != null) {
                if (request.getParameter("task").equals("getUserData")) {
                    String neptunCode = globalNeptunCode;
                    PrintWriter _package = response.getWriter();
                    List<String> data = EgyetemiTudastarModel.getAllData(neptunCode); // Lekérjük az adatokat a service segítségével, listába tároljuk
                    JSONObject obj = new JSONObject(); // Egy JSON objektumba rakjuk (key - value)
                    obj.put("name", data.get(0)); // A "name" alatt a 0. elem lesz meghivatkozva, ami a név 
                    obj.put("neptunCode", data.get(1));
                    _package.write(obj.toString()); // Ezt az objektumot string-é konvertálva küldjük a fooldal Ajax kódjának

                }
                
                if (request.getParameter("task").equals("getSubjectsAndIds")) {
                    String neptunCode = globalNeptunCode;
                    PrintWriter _package = response.getWriter();
                    Map<String, String> map = EgyetemiTudastarModel.getSubjects(neptunCode); // Lekérjük az adatokat a service segítségével, listába tároljuk
                    //List<String> sub = new ArrayList<String>();
                    JSONArray subjects = new JSONArray(); // Egy JSON objektumba rakjuk (key - value)

                    for (Map.Entry<String, String> entry : map.entrySet()) {
                        JSONObject obj = new JSONObject();
                        obj.put("name", entry.getKey());
                        obj.put("id", entry.getValue());
                        subjects.put(obj);
                    }

                    _package.write(subjects.toString()); // Ezt az objektumot string-é konvertálva küldjük a fooldal Ajax kódjának
                }
                
                if (request.getParameter("task").equals("getAllSubjects")) {
                    String neptunCode = globalNeptunCode;
                    PrintWriter _package = response.getWriter();
                    Map<String, String> map = EgyetemiTudastarModel.getApplySubjects(neptunCode); // Lekérjük az adatokat a service segítségével, listába tároljuk
                    //List<String> sub = new ArrayList<String>();
                    JSONArray subjects = new JSONArray(); // Egy JSON objektumba rakjuk (key - value)

                    for (Map.Entry<String, String> entry : map.entrySet()) {
                        JSONObject obj = new JSONObject();
                        obj.put("name", entry.getKey());
                        //System.out.println(entry.getValue());
                        obj.put("id", entry.getValue());
                        subjects.put(obj);
                    }

                    _package.write(subjects.toString()); // Ezt az objektumot string-é konvertálva küldjük a fooldal Ajax kódjának
                }
                
                if (request.getParameter("task").equals("getSubjectsAndIdsAdmin")) {
                    PrintWriter _package = response.getWriter();
                    Map<String, String> map = EgyetemiTudastarModel.getSubjectsAdmin(); // Lekérjük az adatokat a service segítségével, listába tároljuk                   
                    JSONArray subjects = new JSONArray(); // Egy JSON objektumba rakjuk (key - value)

                    for (Map.Entry<String, String> entry : map.entrySet()) {
                        JSONObject obj = new JSONObject();
                        obj.put("name", entry.getKey());
                        obj.put("id", entry.getValue());
                        subjects.put(obj);
                    }

                    _package.write(subjects.toString()); // Ezt az objektumot string-é konvertálva küldjük a fooldal Ajax kódjának
                }
                
                if (request.getParameter("task").equals("getAllUsersAdmin")) {
                    PrintWriter _package = response.getWriter();
                    JSONArray users = EgyetemiTudastarModel.getUsersAdmin(); // Lekért jegyzetek az adatbázisból
                    _package.write(users.toString());
                }
                
                if (request.getParameter("task").equals("getEduCoin")) {
                    String neptunCode = globalNeptunCode;
                    PrintWriter _package = response.getWriter();
                    List<String> data = EgyetemiTudastarModel.getEduCoin(neptunCode); // Lekérjük az adatokat a service segítségével, listába tároljuk
                    JSONObject obj = new JSONObject(); // Egy JSON objektumba rakjuk (key - value)
                    obj.put("level", data.get(0)); // A "name" alatt a 0. elem lesz meghivatkozva, ami a név
                    obj.put("quantity", data.get(1));
                    _package.write(obj.toString()); // Ezt az objektumot string-é konvertálva küldjük a fooldal Ajax kódjának
                }
                
                if (request.getParameter("task").equals("getAllNotes")) {
                    //EgyetemiTudastarModel.educoinUpdate("1");
                    PrintWriter _package = response.getWriter();
                    JSONArray reqNotes = EgyetemiTudastarModel.getSubjectNotes(globalSubjectId); // Lekért jegyzetek az adatbázisból
                    JSONObject object = reqNotes.getJSONObject(0);
                    //System.out.println(reqNotes);
                    _package.write(reqNotes.toString());
                }
                
                if (request.getParameter("task").equals("getGlobalNetunCode")) {
                    PrintWriter _package = response.getWriter();
                    JSONObject obj = new JSONObject(); // Egy JSON objektumba rakjuk (key - value)
                    obj.put("neptun", globalNeptunCode);
                    
                    _package.write(obj.toString());
                }
                
                if (request.getParameter("task").equals("getEduCoinShopData")) {
                    PrintWriter _package = response.getWriter();
                    //Map<String, String> map = EgyetemiTudastarModel.getEduCoinShopData(); // Lekérjük az adatokat a service segítségével, listába tároljuk
                    
                    //JSONArray rewards = new JSONArray(); // Egy JSON objektumba rakjuk (key - value)
                    JSONArray rewards = EgyetemiTudastarModel.getEduCoinShopData(); // Egy JSON objektumba rakjuk (key - value)

                    /*for (Map.Entry<String, String> entry : map.entrySet()) {
                        JSONObject obj = new JSONObject();
                        obj.put("path", entry.getKey());
                        obj.put("price", entry.getValue());
                        rewards.put(obj);
                    }*/
                   
                    _package.write(rewards.toString()); 
                }
                if (request.getParameter("task").equals("checkIfNoteIsRated")) {
                    PrintWriter _package = response.getWriter();
                    boolean canRate = EgyetemiTudastarModel.alreadyRated(globalNeptunCode, globalNoteId);
                    System.out.println("Ertekelhet LoadData: " + canRate);
                    JSONObject obj = new JSONObject();
                    if (canRate) {
                        obj.put("canRate", "true");
                    }
                    else {
                        obj.put("canRate", "false");   
                    }
                    _package.write(obj.toString()); 
                }
                if (request.getParameter("task").equals("getUnlistedSubjects")) {
                    
                }
            }
        } catch (Exception e) {
            System.out.println("Teszt" + e.toString());
            System.out.println(e.getStackTrace()[0]);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processDataRequest(request, response);
        globalSubjectId = request.getParameter("subjectid");
        // Jegyzet �rt�kel�se
        String noteId = request.getParameter("noteId");
        String ratingValue = request.getParameter("userRating");
        globalNoteId = noteId;
        if(noteId != null && ratingValue != null)EgyetemiTudastarModel.storeNoteRating(noteId, ratingValue, globalNeptunCode);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processDataRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
