package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;
import controller.LoadData;
import java.security.NoSuchAlgorithmException;
import model.EgyetemiTudastarModel;

@WebServlet(name = "EgyetemiTudastarController", urlPatterns = {"/Login"})
public class EgyetemiTudastarController extends HttpServlet {

    public static String globalNeptunCode = ""; // Kell egy változó amit a többi fájl is lát, hogy a függvényhez használjuk a neptun kódot

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        globalNeptunCode = request.getParameter("neptunCode");
        String neptunCode = request.getParameter("neptunCode");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();

        try {
            if (EgyetemiTudastarModel.isAdmin(neptunCode, password)) {
                session.setAttribute("authority", "admin");
                session.setAttribute("name", neptunCode);
                RequestDispatcher rd = request.getRequestDispatcher("adminfooldal.html");
                rd.forward(request, response);
                LoadData.processDataRequest(request, response);
            }

            if (EgyetemiTudastarModel.login(neptunCode, password)) {

                if (EgyetemiTudastarModel.isTeacher(neptunCode)) {
                    session.setAttribute("authority", "teacher"); // Ellenőrzi, hogy tanár-e
                } else if (!EgyetemiTudastarModel.isTeacher(neptunCode)) {
                    session.setAttribute("authority", "student");
                }
                session.setAttribute("name", neptunCode);
                RequestDispatcher rd = request.getRequestDispatcher("fooldal.html");
                rd.forward(request, response);
                LoadData.processDataRequest(request, response);
            } else {
                request.getRequestDispatcher("index.html").include(request, response);
                response.getOutputStream().println("<script> " + "document.getElementById(\"loginError\").style.display = \"block\";" + "</script>");
            }
        } catch (NoSuchAlgorithmException ex) {
            System.out.println(ex.toString());
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
