package controller;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.EgyetemiTudastarModel;

@WebServlet(name = "DeleteNote", urlPatterns = {"/DeleteNote"})
public class DeleteNote extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String noteId = request.getParameter("noteId");
        System.out.println("Jegyzet: " + noteId);
        if (noteId != null) {
            EgyetemiTudastarModel.DeleteNoteByNoteid(Integer.parseInt(noteId));
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            TimeUnit.SECONDS.sleep(2);
            request.getRequestDispatcher("jegyzetek.jsp").include(request, response);
        } catch (InterruptedException ex) {
            System.out.println(ex.toString());
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
