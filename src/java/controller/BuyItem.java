package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.EgyetemiTudastarModel;

@WebServlet(name = "BuyItem", urlPatterns = {"/BuyItem"})
public class BuyItem extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String priceOfItem = request.getParameter("priceId");
        if (!priceOfItem.isEmpty()) {
            EgyetemiTudastarModel.DecreaseEducoin(priceOfItem);
        }
    }
}
