package controller;

import static controller.LoadData.globalSubjectId;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.format.DateTimeFormatter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import model.EgyetemiTudastarModel;

@MultipartConfig
@WebServlet(name = "FileUploadServlet", urlPatterns = {"/FileUploadServlet"})
public class FileUploadServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String path = "C:\\Temp\\EgyetemiTudastar\\web\\files";
        //String path = "D:\\Egyetem\\3.félév\\ProgramozásMódszertana2\\Beadandó\\EgyetemiTudastar\\web\\files";
        Part filePart = request.getPart("file");
        String fileName = filePart.getSubmittedFileName();
        long fileSize = filePart.getSize();
        
        if (!fileName.isEmpty() && fileSize != 0) {
            OutputStream os = null;
            InputStream is = null;

            try {
                os = new FileOutputStream(new File(path + File.separator + fileName));
                is = filePart.getInputStream();
                int read = 0;
                while ((read = is.read()) != -1) {
                    os.write(read);
                }
            } catch (FileNotFoundException e) {
                System.out.println(e.toString());
            }

            String subjectid = globalSubjectId;
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String uploaddate = dtf.format(java.time.LocalDateTime.now());
            String fileType = "";
            String noteName = "";
            
            for (int i = fileName.length() - 1; i > 0; i--) {
                char c = fileName.charAt(i);
                int dotIndex = 0;
                if (c == '.') {
                    dotIndex = i;
                    for (int j = dotIndex + 1; j < fileName.length(); j++) {
                        char ch = fileName.charAt(j);
                        fileType += ch;
                    }
                    for (int j = 0; j < dotIndex; j++) {
                        char ch = fileName.charAt(j);
                        noteName += ch;
                    }
                    break;
                }
            }

            EgyetemiTudastarModel.insertNewNote(subjectid, uploaddate, fileType, noteName);
            request.getRequestDispatcher("jegyzetek.jsp").include(request, response);
        } else {
            request.getRequestDispatcher("jegyzetek.jsp").include(request, response);
        }
    }
}
