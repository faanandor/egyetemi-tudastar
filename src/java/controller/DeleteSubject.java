package controller;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.EgyetemiTudastarModel;

@WebServlet(name = "DeleteSubject", urlPatterns = {"/DeleteSubject"})
public class DeleteSubject extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String subjectid = request.getParameter("subjectId");
        System.out.println("Tárgy: " + subjectid);
        if (subjectid != null) {
            EgyetemiTudastarModel.DeleteSubject(subjectid);
            System.out.println("Tárgy méegegyszer: " + subjectid);
        }
    }
}
