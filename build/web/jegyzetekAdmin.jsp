<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="hu">
    <head>
        <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> -->
        <meta charset="UTF-8">
        <title>Egyetemi Tudástár</title>
        <link rel="stylesheet" href="res/style.css">
        <link rel="stylesheet" href="res/all.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
            var rowids = [];
            var noteids = [];
            var selectedRowId = "";
            var selectedNoteId = "";
            var ratingByUser;
            function sendNoteIDandRatingValue() {
                var requestToSend = {"noteId": selectedNoteId, "userRating": ratingByUser};

                $.ajax({
                    url: "/EgyetemiTudastar/LoadData",
                    type: "GET",
                    data: requestToSend,
                    success: function () {
                        //alert("Siker!");
                    },
                    error: function () {
                        //alert("Hiba!");
                    }
                });
            }

            function getNotes() {
                var requestToSend = {"task": "getAllNotes"};
                var requestToSend2 = {"task": "getGlobalNetunCode"};
                var table = document.getElementById("notestable");
                var globalNepunCode;

                $.ajax({
                    url: "/EgyetemiTudastar/LoadData",
                    type: "POST",
                    data: requestToSend2,
                    success: function (response) {
                        globalNepunCode = response.neptun;
                    },
                    error: function () {
                        alert("Hiba!");
                    }
                });

                $.ajax({
                    url: "/EgyetemiTudastar/LoadData",
                    type: "POST",
                    data: requestToSend,
                    success: function (response) {
                        for (var i = 0; i < response.length; i++) {
                            var newRow = table.insertRow(i);
                            var rowId = "row" + i.toString();
                            newRow.setAttribute("id", rowId); // Mindegyik sor kap egy id-t 
                            rowids.push(rowId); // EltÃ¡roljuk a key value pÃ¡rokat egy tÃ¶mbbe
                            // TODO: Upload user rating in database
                            noteids.push(response[i].noteid);
                            //var currentRow = table.rows[index];
                            //newRow.setAttribute("onclick", "alert('document.getElementById('')')");
                            var nameCol = newRow.insertCell(0);
                            var idCol = newRow.insertCell(1);
                            var dateCol = newRow.insertCell(2);
                            var fileCol = newRow.insertCell(3);
                            var neptunCol = newRow.insertCell(4);
                            var authorityCol = newRow.insertCell(5);
                            var ratingCol = newRow.insertCell(6);
                            nameCol.innerHTML = response[i].name;
                            nameCol.setAttribute("class", "idSelector");
                            newRow.setAttribute("id", rowId); // RowID az elsÅ cellÃ¡nak
                            idCol.innerHTML = response[i].subjectid;
                            dateCol.innerHTML = response[i].uploaddate;
                            fileCol.innerHTML = response[i].file;
                            neptunCol.innerHTML = response[i].userid;
                            authorityCol.innerHTML = response[i].isTeacher;
                            var numOfStars = parseInt(response[i].ratingValue); // KiszedjÃ¼k a csillagok szÃ¡mÃ¡t
                            console.log("Hello: " + numOfStars);
                            var userRating = "rating" + i.toString();
                            ratingCol.innerHTML += "<div class='rating' id='" + userRating + "'>";
                            var myDiv = document.getElementById(userRating);
                            var p = document.createElement("p");
                            var remainingStars = 5 - numOfStars;
                            if (remainingStars > 0) {
                                for (var j = 0; j < remainingStars; j++) {
                                    var myDiv = document.getElementById(userRating);
                                    var starValue = remainingStars + 1;
                                    var inpTag = document.createElement("input");
                                    var lbl = document.createElement("label");
                                    p.setAttribute("id", "starNotFilled");
                                    lbl.setAttribute("for", "starNotFilled");
                                    myDiv.appendChild(p);
                                    myDiv.appendChild(lbl);
                                }
                            }
                            if (numOfStars > 0) {
                                for (var j = 0; j < numOfStars; j++) {
                                    var myDiv = document.getElementById(userRating);
                                    var starValue = j + 1;
                                    var p = document.createElement("p");
                                    var lbl = document.createElement("label");
                                    p.setAttribute("id", "starFilled");
                                    lbl.setAttribute("for", "starFilled");
                                    myDiv.appendChild(p);
                                    myDiv.appendChild(lbl);
                                }
                            }

                            var rateCol = newRow.insertCell(7);
                            rateCol.innerHTML += "<td><button class='deletebtn' onclick='getIDandDelete(this);'>Törlés</button></td>";
                        }
                    },
                    error: function () {

                    }
                });
            }

            function delay(URL) {
                setTimeout(function () {
                    window.location = URL;
                }, 150);
            }
        </script>
    </head>
    <body onload="getNotes()">  
        <header> 
            <!--eleje vÃ¡ltoztatÃ¡s 1 -->
            <div class="notesh3">
                <h3>Jegyzetek</h3>
            </div>     
            <!--VÃ©ge vÃ¡ltoztatÃ¡s 1-->      
            <div class="cancel">
                <a href="adminfooldal.html">X</a>
            </div>
        </header>  

        <main>
            <table class="notestable">
                <thead>
                    <tr>
                        <th>Név</th>
                        <th>Kód</th>
                        <th>Dátum</th>
                        <th>Típus</th>
                        <th>Feltöltő</th>
                        <th>Tanár/Diák</th>
                        <th>Értékelés</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="notestable">

                </tbody>
            </table>
            
        </main>

        <!--DELETE CONFIRM POPUP-->
        <div id="popupdeleteconfirm">
            <div class="content">
                <img src="res/ok.png">
                <h3>Biztosan törli a jegyzetet?</h3>
                <div class="inputBox">
                    <!-- <form method="POST" action="DeleteNote">
                    </form> -->
                    <input type="submit" value="Igen" class="deleteconfirmbtn" onclick="popupToggleDeleteConfirm();">
                    <input type="button" value="Mégse" class="deleteconfirmbtn" onclick="popupToggleDelete();">
                </div>
            </div>
        </div>
        <script>
            function popupToggleDeleteConfirm() {
                //const popupdeleteconfirm = document.getElementById('popupdeleteconfirm');
                //popupdeleteconfirm.classList.toggle('active');
                var requestToSend = {"noteId": selectedNoteId};

                $.ajax({
                    url: "/EgyetemiTudastar/DeleteNote",
                    type: "GET",
                    data: requestToSend,
                    success: function () {
                        delay("jegyzetekAdmin.jsp");
                    },
                    error: function () {
                        //alert("Hiba!");
                    }
                });
            }

            function getIDandDelete(element) {
                myIndex = (element.parentNode.parentNode.rowIndex) - 1;
                selectedRowId = "row" + myIndex.toString();

                indexOfRow = rowids.indexOf(selectedRowId);
                selectedNoteId = noteids[indexOfRow];

                popupToggleDelete();
            }


            function popupToggleDelete() {
                const popupdeleteconfirm = document.getElementById('popupdeleteconfirm');
                popupdeleteconfirm.classList.toggle('active');
            }
        </script>

    </body>
</html>
